-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Nov 2018 pada 19.28
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neotrade_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transactions`
--

CREATE TABLE `detail_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `homepages`
--

CREATE TABLE `homepages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `images_product`
--

CREATE TABLE `images_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(54) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_01_203801_create_homepages_table', 1),
(4, '2018_11_04_170925_create_products', 1),
(5, '2018_11_04_172024_create_images_product', 1),
(6, '2018_11_04_172812_create_transactions', 1),
(7, '2018_11_04_172907_create_detail_transactions', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_code` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `date_transaction` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_payment` enum('waiting','success','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `proof_of_payment` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `api_token`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$F.76XZsTVB.pOuex5bc2uuXpYDghu8nPmaOiprL4gOa5qx8ZSrrCW', 'v3VtIrv37StLo1G0nK3yd8j5', 1, 'GRUXlRk5934OHwmN', NULL, NULL),
(2, 'vharyanto', 'cgunarto@example.com', '$2y$10$oAsjvAlp4CkPLbD4J/wgd.gKEYBHn.Ehl8PX.igoP6oOEx9SqcwuK', 'WCZwYliKM8TnWddYSfK9oda2', 0, '4eyAX2ock2QXskov', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(3, 'firmansyah.hasim', 'siska.mardhiyah@example.org', '$2y$10$ql9/vvdTHkHmjnSZna066OWwnK0utpyKvL27paVJQBkHEyNrTj8qy', 'DBkfkqYiZEFvgz0e1piPTnQd', 0, 'y5Tm2T8WeMaqaCEV', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(4, 'putri29', 'rahmawati.novi@example.net', '$2y$10$ngqxsyjjFrkcsxl3mkjFCehbxOtLvXJ.CUugEHzFZEIexN0IxdsB6', 'fajJIPJ7RaxaaUHynR4OuHC9', 0, 'N7E7UJCwkxyAv3iy', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(5, 'gawati62', 'ina.gunarto@example.net', '$2y$10$jcRG4bdWYRK6zOO8VYRfZuXbVUwwVcQBj3x/u2J/4V3W5bkN/27vO', 'oEKwwLlnnq2HTdv7xOlrejSG', 0, 'QSxDx6Rf5pQxaFJx', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(6, 'daru.budiman', 'lutfan13@example.org', '$2y$10$W4kgikzHSNOmGI5FY5PLW..6fXyQjLxuMyNETm27wRx6u19OWtFCK', 'jACG5FCEJIC1irNCY5fjRw4x', 0, '5pGZ386lgzj3s41L', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(7, 'zulaika.siti', 'arsipatra54@example.com', '$2y$10$bRAYZzTO8GU50kzOmn.95.X7b/xkUGd.CuVYg5wlZyZKrfDDd/Jg2', 'xKeZTnZALsgNF2UWI1TgnWMX', 0, 'ruz21MExP0UEg5Xa', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(8, 'sarah.kuswandari', 'jhandayani@example.org', '$2y$10$4JP34HxPFXK0NhSJMMNRq.fgXrSrZqueU0EbOBR6Kt8P0bOV5ZOmS', 'iQsO5rcywmJJu2PQtzEfSAC6', 0, 'TGVmCqE7N7V9Xebf', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(9, 'wardaya.saptono', 'almira34@example.com', '$2y$10$HdnXXBz10vODT.RiJ2j77.Cf9.etE0KM2ognF1KQlhQHKsmGOxyBy', 'A4EQRWhN68csMH890rxCY47M', 0, 'ze9vCd0eIH94ZOU9', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(10, 'queen.kusumo', 'citra70@example.com', '$2y$10$yNL7J7zPWZNFV9YM6AxUA.jzdw11tRCVw7R6ZS9d7eHVPmyPGGdPW', 'IRDpzEcxoxFVRydi4xHNLrAl', 0, 'ESMq6Xk2mvCufWOI', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(11, 'yuliarti.kartika', 'marpaung.banawa@example.org', '$2y$10$rNOzH0jExsX6EyoIgdwJHeA5C8ldUS88ZuqT47Hc25fck05uMR4QS', 'c4QX3riu258ltAx5KemnwIzj', 0, 'yqfHTG71Veemq9vy', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(12, 'rnajmudin', 'gadang45@example.org', '$2y$10$Dlk/sxNT21hk1jagJSW2OOj7PWHOKvuNWIpJcIZqC2Bx3NO7bfAIq', 'opFFRlyyrLOAqphy6F8Xvoxl', 0, 'EZXCf6Y2yd0GTALl', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(13, 'hidayanto.hardana', 'ikusmawati@example.net', '$2y$10$jzrBdRoDVp6S7uKoxH7mneQrJ140WdnkkJMkO6JHgy928GqIGSmXS', 'DeR8IHvLibB3j9ssqiLNGDHa', 0, 'BVsOejcuG0EKBwUu', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(14, 'aisyah.wibisono', 'ayu24@example.org', '$2y$10$WIROemkcGGktwSNvBOECpOhA/mKnRaZO37gkl0Db.TGrNiTQCFDGi', '5CRea9NvxkpWgWAqvl8VQ5yT', 0, 'ipdbeT2UJ42PuSP8', '2018-11-04 11:27:28', '2018-11-04 11:27:28'),
(15, 'jamal.palastri', 'calista58@example.net', '$2y$10$PGuW9LYGyWpx5XUQ6JXgGO5bl9GJwmxOBtZFEoIiclgQ0WTz2LM1O', 'ZMrKsE3TsVA6uldB7HZ0AT1H', 0, 'rwr0Jpbchqttl6Tz', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(16, 'titin.rahayu', 'cpradana@example.net', '$2y$10$Lxgz6Df7sH2mbJzvz9bi5.V5Dc5cwCRux3wD2ojztuHq/EF/rqNZi', 'bZPyrNropmkiUxy0AeB0CXMX', 0, 'JdpCdzhcUEEgbWUm', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(17, 'waluyo.bakijan', 'zaenab.prasasta@example.org', '$2y$10$Ep/cQaDDb.9CQRlYtgwg1ecYPGNgAIca6wdJZdt176Mm0cm3oXAFS', 'RUybdGVqbv5vzRtxUN27NzHj', 0, 'rAhtY320YvszjVzr', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(18, 'firmansyah.syahrini', 'suryono.caket@example.com', '$2y$10$9BM/3KHFAX5UQayavxibluUi.TyRz6ZpgO1G7buZgdeUMeq4/LUcS', 'u7bTxtHRxnl9VThUYy6JQoeY', 0, 'KMfphNEgFTjDDKgH', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(19, 'simbolon.ajeng', 'rahman37@example.org', '$2y$10$BjEBKT/OYnyIErF14o9xUeo.oA4A/V6rzfL6MEQFRjmbxhjkfMm0i', 'SlOIKsVlrAJ5lHbjdpRzQChp', 0, 'VrddBhiaF5oeZGiV', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(20, 'giswahyudi', 'eka44@example.net', '$2y$10$ZskqxtQ6CxMmB9jXnrljBuYNXslpMwEJXEuImwh5df0qZ.H29uPaG', 'Z00rwjkAtPQOqRdzpEuvQuPY', 0, 'bfLhVD0e72UpI6YX', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(21, 'rnurdiyanti', 'lharyanto@example.org', '$2y$10$GduIpTWTitKzMDJRjV07f.KJcrp/eCS9lKlYdQVhQBFosuTtiM.yG', 'Cohy9E9cGHnEk5ENcK95gd4o', 0, 'loPv83MKX8bROsy9', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(22, 'dwi.sitompul', 'mustofa43@example.net', '$2y$10$Pa/d2lTS05Uzsm1IyJ68YePi3iUqacta0naKyl3NESHaBjrwSkt/i', 'w2X2aIBYwCMoBD6apbR9H0Em', 0, '1uiGXquWjSMOUn3i', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(23, 'wahyudin.harto', 'opudjiastuti@example.net', '$2y$10$P7HNcV5rZ4AU06/khFTT3OycN7uVkq2wXxD/ZxuPmMwLc5neaMjkK', '7jy65fgyeE03dUagRPqHWLG4', 0, 'E41fG0sA7lJDb28X', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(24, 'gilda.fujiati', 'gyolanda@example.org', '$2y$10$njQxCX46n2S49L/vI1C2zuEF6u8aTNQhlvedgl6mcsnBTpD2I5XEK', 'hhv0jiKSiBzsq8a87pYgdoKu', 0, 'tYqxE3fWgTFOUsoy', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(25, 'prasetyo.radika', 'oliva.hardiansyah@example.net', '$2y$10$32cs10C1ieCsIYakJjHa3eLC5GxFHuv1a8CIerp3g5MoxkdAINPDG', 'ZbFB1XW5coB7l0PHnusXzKNc', 0, 'BkE0jAsBa6lZ1WLX', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(26, 'lanang.yuniar', 'lzulaika@example.com', '$2y$10$A4fMkPfOopIjJXBC9Lqi7uwa6iRba8Z2hz9IH7y6IcMO5qW3UgoEC', 'YWjKC8f5LzweCYvRy3U3IM6r', 0, 'UPEPIddGRR0tHcxz', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(27, 'whartati', 'epermata@example.org', '$2y$10$ZRezCAacDIKQILrj6TvOv.g20pI0shzMOOkWo63sntrboPBA0zUoe', '2HSfUi6uSKGES9N0y6WcCOl7', 0, 'd1XqCYe359gQ97pT', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(28, 'sinaga.dimas', 'bdabukke@example.com', '$2y$10$bUowvRQzIAw0AJVMN3B5mOZFNFHMTKcAiJF18FXLNc8OsZMSk5uNO', 'jUSTpmbG1OWBqQPqHg15eLmC', 0, '7NF6kfEAcJp0YPUJ', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(29, 'shakila.pertiwi', 'ganjaran.mustofa@example.com', '$2y$10$/0vNF.5IBBez/RA7nAsumO2QaiRrE1wltTccq9R9cWydLozSaEcVa', '1NEU1vaK6LzKwMdfenvDw1OT', 0, 'PzE51LenMBLKLTif', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(30, 'puji20', 'aswani.nashiruddin@example.net', '$2y$10$s9/pvTmbUYX8g9xw/L.OMOfeGgusq80edkjclG/n.jF/L9ojEJscy', 'KrxbCl5zmJZlHL4BBZ9PbM6d', 0, 'mZ8ejKi62yRFePR8', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(31, 'yfirgantoro', 'oliva93@example.net', '$2y$10$Ta0V8gYK8U5dPI5AKLb1z.w/Wz0Py4SRDbKS1FbJ66if/d4YtR2ca', 'CNzlditMD30yRCqYWMtzeTsd', 0, 'muZuB8oPPEqRvIMO', '2018-11-04 11:27:29', '2018-11-04 11:27:29'),
(32, 'oman.rajasa', 'laras27@example.com', '$2y$10$v9QGrbTZrGgkwWYlp/OJhOIk2/1YOqtWb6M.x3PL.RpT.RMm6jhrK', 'HszoRqBUedIDcXdriueU92Oz', 0, 'CwNkk6dpbE98WNjX', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(33, 'kadir.puspita', 'danu.wibowo@example.net', '$2y$10$WeZbf4lghuIs9IU83c/WyeKV38CsfFC/jOGtZriLf/8zpCVlY2C7m', 'OdmnuAL6FX346Lp90Ooh7AVI', 0, 'Jd3DfBxOD6Dk1lYH', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(34, 'dongoran.aisyah', 'vsafitri@example.org', '$2y$10$29sxkX0VA2Fnka5XmacfeOZ9kSmdmuAiWDj0PWtTQ5fTnJ5AMJlH2', 'mWWnm6pStwCevO78Z86UKZ3r', 0, 'xoOUKvtcRRoV8G0G', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(35, 'firgantoro.elma', 'keisha.wahyuni@example.org', '$2y$10$9WIkHUg7kFxsf2.67fgFqujaQhWNjiPKayMJ8XqJ0cJAO8mYTl5sW', 'OMYe7jZ5yd6aXPZpzPEjL4ko', 0, 'eGaLjvIH73OWlH0e', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(36, 'ukusumo', 'ssiregar@example.com', '$2y$10$dKWC3KXwkz5xGrZidcUTlu7Sl1f4zdAF/kKXwkfgWQG7HoeIBA8Zm', 'pr89BhahDfVtnBDzNTL3Y9rx', 0, 'xA7JF7MbLaareMEs', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(37, 'darmaji57', 'xnasyidah@example.net', '$2y$10$cxBEXIlBEDHVlhI5BrSny.v9YzVz5Letg831..kU3VrkZb.SsAO9q', 'AMkm9CkXApBRmSDSD9ZKtsId', 0, 'ccLts2Agt8NpkgBR', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(38, 'widya.maryati', 'yani81@example.com', '$2y$10$CsAs/VLYoH5kETkZp5OfH.01/cnYKWWOViTnav3LMNH1.J04sVWfq', 'JszpUUfklgXitCkFbmH41aDH', 0, 'uUYPurWYssBpxJRk', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(39, 'cdabukke', 'thutagalung@example.net', '$2y$10$C7MNYPSyD5SwFZWNhMMtLu13B.HmnCoaLabbSHVoWF4CjcEx65FEC', 'bi0yFT8IRV8ihO1yeN4hbF8h', 0, 'kTBlgRSs6LWsfUBX', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(40, 'oktaviani.candra', 'purnawati.sari@example.org', '$2y$10$6vdtPWz8J7r7O0BCyCq4ru3wex1LJzM7u6XfPZ4weVw3Az8GqKvcK', 'WfPHkBjHrbNRAZvmB9t8dUhy', 0, 'SxxZ6IUviYgtihOf', '2018-11-04 11:27:30', '2018-11-04 11:27:30'),
(41, 'zalindra08', 'talia.hastuti@example.org', '$2y$10$8g8DF6FQD1GR.Cg3Z0x5j.TDJokH9xMjcWA6fXLm4nJB.13RVbbCa', 'bt5DNozNBJc5FURc8C9twyHe', 0, '0z08zYNYwmAnW06A', '2018-11-04 11:27:30', '2018-11-04 11:27:30');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transactions_transaction_id_foreign` (`transaction_id`),
  ADD KEY `detail_transactions_product_id_foreign` (`product_id`);

--
-- Indeks untuk tabel `homepages`
--
ALTER TABLE `homepages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `images_product`
--
ALTER TABLE `images_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_product_product_id_foreign` (`product_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `homepages`
--
ALTER TABLE `homepages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `images_product`
--
ALTER TABLE `images_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  ADD CONSTRAINT `detail_transactions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `detail_transactions_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`);

--
-- Ketidakleluasaan untuk tabel `images_product`
--
ALTER TABLE `images_product`
  ADD CONSTRAINT `images_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Ketidakleluasaan untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

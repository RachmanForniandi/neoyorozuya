<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     //    $users = [
    	// 	'username' => 'admin',
	    //     'email' => 'admin@admin.com',
	    //     'password' => bcrypt('admin'), 
	    //     'remember_token' => str_random(16),
	    //     'api_token' => str_random(24),
	    //     'is_admin' => true,
    	// ];
    	$this->call(UsersTableSeeder::class);
    }
}
